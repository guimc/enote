const DOT_MRANG = 11;

const INT_CON_MRANG = 10;
const INT_END_MRANG = 11;
const INT_PERP_MRANG = 12;

const CIR_CON_MRANG = 10;
const CIR_QUAD_MRANG = 11;


function distToPoint(pX, pY, x, y) {
    return Math.sqrt(Math.pow(pX - x, 2) + Math.pow(pY - y, 2));
}

function tryMagnetDot(result, dotObj, x, y, z, rang) {
    let dist = 0;
    if (y) dist = distToPoint(dotObj.x.val, dotObj.y.val, x, y);
    if (z) dist = distToPoint(dotObj.x.val, dotObj.z.val, x, z);

    result.cdist = dist;
    if (dist < DIST_FILTER && dist < result.dist && result.rang < rang) {
        result.magneted = true;
        if (dotObj.x && x) result.x = dotObj.x.val;
        if (dotObj.y && y) result.y = dotObj.y.val;
        if (dotObj.z && z) result.z = dotObj.z.val;
        result.dist = dist;
        result.rang = rang;

        return true;
    }
    return false;
}

function tryMagnetInterval(result, intObj, x,y,z, curObject) {
    if (curObject.type == "interval" && curObject.keyPoints && curObject.keyPoints.interval && curObject.keyPoints.interval.start.x) {
        let vX1 = intObj.end.x.val - intObj.start.x.val;
        let vY1 = (y) ? intObj.end.y.val - intObj.start.y.val : intObj.end.z.val - intObj.start.z.val;
        let vL1 = Math.sqrt(Math.pow(vX1, 2) + Math.pow(vY1, 2));

        let vX1n = vX1 / vL1;
        let vY1n = vY1 / vL1;

        let vX2 = curObject.keyPoints.interval.start.x.val - intObj.start.x.val;
        let vY2 = (y) ? curObject.keyPoints.interval.start.y.val - intObj.start.y.val : curObject.keyPoints.interval.start.z.val - intObj.start.z.val;
        let vL2 = Math.sqrt(Math.pow(vX2, 2) + Math.pow(vY2, 2));

        let cosPhi = (vX1n*vX2 + vY1n*vY2)/vL2;
        let vL3 = vL2*cosPhi;

        let pX = intObj.start.x.val + vX1n*vL3;
        let pY = (y) ? intObj.start.y.val + vY1n*vL3 : intObj.start.z.val + vY1n*vL3;

        let dist = 0;
        if (y) dist = distToPoint(pX, pY, x, y);
        if (z) dist = distToPoint(pX, pY, x, z);
    
        result.cdist = dist;
        if (dist < DIST_FILTER && dist < result.dist && result.rang < INT_PERP_MRANG) {
            result.magneted = true;
            if (x) result.x = pX;
            if (y) result.y = pY;
            if (z) result.z = pY;
            result.dist = dist;
            result.rang = DOT_MRANG;

            return true;
        }
    }

    return false;
}


function tryMagnetCirQuads(result, cirObj, x, y, z) {
    let quadDot = {x:{val:cirObj.x.val+cirObj.r.val}, y:{val:cirObj.y.val}, z:{val:cirObj.z.val}};
    let magnetedQuad = tryMagnetDot(result, quadDot, x, y, z, CIR_QUAD_MRANG);

    if (!magnetedQuad) {
        quadDot = {x:{val:cirObj.x.val-cirObj.r.val}, y:{val:cirObj.y.val}, z:{val:cirObj.z.val}};
        magnetedQuad = tryMagnetDot(result, quadDot, x, y, z, CIR_QUAD_MRANG);
    }

    if (!magnetedQuad) {
        if (y) quadDot = {x:{val:cirObj.x.val}, y:{val:cirObj.y.val-cirObj.r.val}, z:{val:cirObj.z.val}};
        else quadDot = {x:{val:cirObj.x.val}, y:{val:cirObj.y.val}, z:{val:cirObj.z.val-cirObj.r.val}}; 
        magnetedQuad = tryMagnetDot(result, quadDot, x, y, z, CIR_QUAD_MRANG);
    }
    
    if (!magnetedQuad) {
        if (y) quadDot = {x:{val:cirObj.x.val}, y:{val:cirObj.y.val+cirObj.r.val}, z:{val:cirObj.z.val}};
        else quadDot = {x:{val:cirObj.x.val}, y:{val:cirObj.y.val}, z:{val:cirObj.z.val+cirObj.r.val}}; 
        console.log("c", cirObj, "q", quadDot);
        magnetedQuad = tryMagnetDot(result, quadDot, x, y, z, CIR_QUAD_MRANG);
    }

    return magnetedQuad;
}
function tryMagnetCirConn(result, cirObj, x, y, z) {
    let radVect = {
        x: x-cirObj.x.val,
        y: (y) ? y-cirObj.y.val : z-cirObj.z.val
    }

    let radVectLen = Math.sqrt(Math.pow(radVect.x, 2) + Math.pow(radVect.y, 2));

    let radDot = {
        x: {val: cirObj.x.val+radVect.x/radVectLen*cirObj.r.val}
    }
    if (y) radDot.y = {val: cirObj.y.val+radVect.y/radVectLen*cirObj.r.val};
    else radDot.z = {val: cirObj.z.val+radVect.y/radVectLen*cirObj.r.val};

    return tryMagnetDot(result, radDot, x, y, z, CIR_CON_MRANG);
}


function tryMagnetCircle(result, cirObj, x, y, z) {
    tryMagnetCirQuads(result, cirObj, x, y, z);
    tryMagnetCirConn(result, cirObj, x, y, z);
}

function tryMagnetElement(result, keyObj, x, y, z, curObject) {
    switch (keyObj.type.val) {
        case 'dot':
            tryMagnetDot(result, keyObj, x, y, z, DOT_MRANG);
            break;
        case 'interval':
            tryMagnetInterval(result, keyObj, x, y, z, curObject);
            break;
        case 'circle':
            tryMagnetCircle(result, keyObj, x, y, z, curObject);
            break;
    }

    return result;
}
