var canvasElement;
var curObject = {};
var task = {
    step: 1,
    stepsAmount: 1,
    stepTexts: [''],
    objects: []
}

// Начальное положение инструмента
const instrument = {
    type: "plane",
    stage: 0, // договоримся, что клик внутри инструмента называется "стадия-stage", а шаги в задаче (когда на шаге добавляется новое построение) - называть будем "шаг-step"
    prevStagePlane: 0
}


// Функция восстановления инструмента в начальное положение, с которого начинается с ним работа
function resetInstrument() {
    instrument.stage = 0; // шаг становится нулевым
    instrument.prevStagePlane = 0;

    // для элемента на странице с id editorHint задаём текст (innerText)
    document.getElementById("editorHint").innerText =
        drawInstruments[instrument.type].stages[0].hint; // берём из арсенала тот инструмент, у которого имя instrument.type и выводим подсказку его первого шага
}

function setPointElementByPath(obj, path, value) {
    let subObj = obj.keyPoints;
    let splitPath = path.split('.');
    for (let index = 0; index < splitPath.length - 1; index++) {
        const pathElem = splitPath[index];

        if (!subObj[pathElem])
            subObj[pathElem] = {};
        subObj = subObj[pathElem];
    }
    subObj[splitPath[splitPath.length - 1]] = value;
    //console.log('set', path, 'to', value, obj);

    return subObj[splitPath[splitPath.length - 1]];
}
function getPointElementByPath(obj, path) {
    let subObj = obj.keyPoints;
    let splitPath = path.split('.');
    for (let index = 0; index < splitPath.length - 1; index++) {
        const pathElem = splitPath[index];

        if (!subObj[pathElem])
            subObj[pathElem] = {};
        subObj = subObj[pathElem];
    }
    return subObj[splitPath[splitPath.length - 1]];
}


// Обработка стадии инструмента, расчёт и запись получившихся значений из клика
function processInstrumentStage(x, y, z, plane, isClicked) {
    const curInstrument = drawInstruments[instrument.type];
    const curInstrumentStage = curInstrument.stages[instrument.stage];

    if (instrument.stage == 0)
        curObject = { type: instrument.type, stage: 0, keyPoints: {}, keyPathSet: [] };
    
    //console.log("smP", curInstrumentStage ,instrument, curInstrumentStage.samePlane, plane, instrument.prevStagePlane, curInstrumentStage.samePlane > 0 , plane != instrument.prevStagePlane)
    if (curInstrumentStage.samePlane > 0 && plane != instrument.prevStagePlane) return;
    if (curInstrumentStage.samePlane < 0 && plane == instrument.prevStagePlane) return;
    
    //console.log(curInstrumentStage.samePlane, plane, instrument.prevStagePlane, curInstrumentStage.samePlane > 0, plane !== instrument.prevStagePlane)

    let magnet = magneticSearch(x, y, z, plane);
    if (magnet.magneted) {
        x = magnet.x;
        y = magnet.y;
        z = magnet.z;
    }

    let setOnThisProcess = [];

    //console.log("SETS");
    curInstrumentStage.sets.forEach(set => {
        //console.log('startSet', set);
        if (!(
                getPointElementByPath(curObject, set.keyPath + '.set') ||
                setOnThisProcess.includes(set.keyPath)
            )) {
            setPointElementByPath(curObject, set.keyPath + '.val', null);
        }
        
        //console.log(set, "Pl, forP", plane, set.forPlane, set.forPlane, plane == set.forPlane , set.forPlane == 0)
        
        if (plane == set.forPlane || set.forPlane == 0) {
            //console.log('gonna set', set.keyPath + '.val', eval(set.value))
            
            setPointElementByPath(curObject, set.keyPath + '.val', eval(set.value));
            setPointElementByPath(curObject, set.keyPath + '.set', isClicked);

            setOnThisProcess.push(set.keyPath);
        }
        
        //console.log('endSet', set);
    });
    //console.log("SETS_END");



    if (isClicked) {
        instrument.prevStagePlane = plane;
        curObject.stage = instrument.stage;
        instrument.stage++;

        if (instrument.stage >= curInstrument.stages.length) {
            curObject.step = task.step;
            task.objects.push(curObject);
            curObject = {};
            resetInstrument();
            document.getElementById("taskDisplay").innerText = JSON.stringify(task, null, 2);

            updateIframeCode();
        } else {
            document.getElementById("editorHint").innerText = drawInstruments[instrument.type].stages[instrument.stage].hint;
        }
    }

    document.getElementById("curObjectDisplay").innerText = JSON.stringify(curObject, null, 2);
    document.getElementById("curInstrumentDisplay").innerText = JSON.stringify(instrument, null, 2);

    redrawEditorCanvas();
}

const DIST_FILTER = 12;
function magneticSearch(x, y, z, plane) {
    let result = {
        magneted: false,
        x: x,
        y: y,
        z: z,
        plane: plane,
        cdist: 0,
        dist: 99999,
        rang: 0
    }

    task.objects.forEach(taskObject => {
        if (taskObject.step <= task.step) {
            for (const [name, keyObj] of Object.entries(taskObject.keyPoints)) {
                result = tryMagnetElement(result, keyObj, x, y, z, curObject);
            };
        }
    });

    document.getElementById("magnetedDisplay").innerText = JSON.stringify(result, null, 2);

    return result;
}

// Обработка клика. Главная задача - определить на какую плоскость попал клик (верхняя или нижняя, для Z или для Y)
// Параллельно координаты пересчитываются из экранных в систему координат XYZ (3d)
// Чтобы не запутаться, стоит определиться с системой хранения координат как можно раньше и придерживаться её
// Хотя большинство нашей работы пройдет именно в экранных координатах, я хочу сразу хранить все в 3д.
// Придётся часто заниматься переводом одного в другое, но я надеюсь что скажу себе зпасибо за это решение тогда, когда мы дойдем до 3д вида
// Может я передумаю в ближайшее время, пока ещё не сликом поздно=)
function mouseProcessor(event, isClicked) {
    let x = canvasElement.clientWidth - FRAME_OFFSET - event.offsetX;
    let y, z;
    let plane = 0;

    if (event.offsetY < canvasElement.clientHeight / 2) {
        plane = 2;
        y = null;
        z = canvasElement.clientHeight / 2 - event.offsetY;
    } else {
        plane = 1;
        y = event.offsetY - canvasElement.clientHeight / 2;
        z = null;
    }

    
    processInstrumentStage(x, y, z, plane, isClicked);
}



function redrawEditorCanvas() {
    redrawCanvas();
    drawObject(curObject, true);
}





function updateStepCounter() {
    document.getElementById("stepCounter").innerText = "Шаг: " + task.step; //где ... - номер шага
    document.getElementById("taskDisplay").innerText = JSON.stringify(task, null, 2);
    document.getElementById("stepText").value = task.stepTexts[task.step-1];

    if (task.step === 1) {
        document.getElementById("backButton").disabled = true;
    } else {
        document.getElementById("backButton").disabled = false;
    }

    if (!task.stepTexts[task.step]) task.stepTexts[task.step] = '';

    /*Код для проигрывателя
        if (task.step === task.stepsAmount) {
            document.getElementById("nextButton").disabled = true;
        } else {
            document.getElementById("nextButton").disabled = false;
        }
    */

    redrawEditorCanvas();
}

const IFRAME_TEMPLATE = `
<div class="bie-module-iframe-container" id="testingIframeContainer">
<iframe class="bie-module-iframe" scrolling="no" frameBorder="0" srcdoc='
    <!DOCTYPE html>
    <html>

    <head>
    <title>Разбор задачи. Электронная Рабочая Тетрадь по Начертательной Геометрии</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="player-styles.css" />
    <script>
        var task = {task-obj};
    </script>
    <script src="common.js"></script>
    </head>

    <body class="bie-module-iframe-body">
        <canvas id="drawingPlane" class="bie-module-iframe-canvas"></canvas>
        <div id="playerStepHint">Комментарий к шагу</div>
    </body>

    </html>'>
</iframe>
</div>
`;
function updateIframeCode() {
    let html = IFRAME_TEMPLATE.replace(/{task-obj}/, JSON.stringify(task)); // Вставляет в шаблон пользовательского iframe наш текущий объект task,
                                                                            // превращая его в текст с помощью JSON.stringify
    console.log(html);
    document.getElementById("playerArea").innerHTML = html; // Получившийся HTML код вживляется внутрь элемента #playerArea
    document.getElementById("iframeCodeShare").value = html;
};



// Для действий, которые происходят после загрузки страницы
addEventListener("DOMContentLoaded", (event) => {
    // Подготовительные действия по нахождению элемента для рисования и его запоминания.
    canvasElement = document.getElementById('drawingPlane');

    canvasElement.addEventListener('click', (event) => { mouseProcessor(event, true); });
    canvasElement.addEventListener('mousemove', (event) => { mouseProcessor(event, false); });


    document.getElementById("dotbutton").addEventListener('click', (event) => { instrument.type = "dot"; resetInstrument(); })
    document.getElementById("intervalbutton").addEventListener('click', (event) => { instrument.type = "interval"; resetInstrument(); })
    document.getElementById("conebutton").addEventListener('click', (event) => { instrument.type = "cone"; resetInstrument(); })
    document.getElementById("planebutton").addEventListener('click', (event) => { instrument.type = "plane"; resetInstrument(); })
    document.getElementById("pplanebutton").addEventListener('click', (event) => { instrument.type = "pplane"; resetInstrument(); })


    document.getElementById("stepText").oninput = function() {
        task.stepTexts[task.step-1] = document.getElementById("stepText").value;
        document.getElementById("taskDisplay").innerText = JSON.stringify(task, null, 2);
    };


    document.getElementById("backButton").addEventListener("click", () => {
        if (task.step > 1) {
            task.step--;
            updateStepCounter();
            // Дополнительный код для перехода к предыдущему шагу
        }
    });

    document.getElementById("nextButton").addEventListener("click", () => {
        task.step++;
        if (task.step > task.stepsAmount) task.stepsAmount = task.step;
        updateStepCounter();


        /* Этот код будет для проигрывателя
        if (task.step < task.stepsAmount) {
            task.step++;
            updateStepCounter();
            // Дополнительный код для перехода к следующему шагу
        }
        */
    });

    updateStepCounter();
    resetInstrument();
});