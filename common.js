const FRAME_OFFSET = 10;

var canvasElement;
var ctx;

const LINE_STYLE_SOLID = [];
const LINE_STYLE_DASHED = [10, 5];

var POINT_SIZE = 3;
function drawPoint(x, y) {
    ctx.fillRect(x - POINT_SIZE / 2, y - POINT_SIZE / 2, POINT_SIZE, POINT_SIZE);
}

function drawLine(x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
    ctx.closePath();
}

function drawCircle(centerX, centerY, radius) {
    ctx.beginPath();
    ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    ctx.stroke();
    ctx.closePath();
};

function redrawCanvas() {
    ctx.clearRect(0, 0, canvasElement.width, canvasElement.height); // очищаем плоскость от всего старого

    drawPlaneLine();
    
    task.objects.forEach(object => {
        if (object.step <= task.step) drawObject(object);
    });
}



function toScrX(x) {
    // x = canvasElement.clientWidth - FRAME_OFFSET - event.offsetX;
    return canvasElement.clientWidth - FRAME_OFFSET - x;
}
function toScrY(y) {
    // y = event.offsetY - canvasElement.clientHeight/2;
    return y + canvasElement.clientHeight / 2;
}
function toScrZ(z) {
    // z = canvasElement.clientHeight/2 - event.offsetY;
    return canvasElement.clientHeight / 2 - z;
}



function tryDrawPoint(dot, obj) {
    let scrX, scrY, scrZ;
    //console.log(dot);

    scrX = toScrX(dot.x.val);

    if (dot.y && dot.y.val) {
        scrY = toScrY(dot.y.val);
        drawPoint(scrX, scrY);
    }
    if (dot.z && dot.z.val) {
        scrZ = toScrZ(dot.z.val);
        drawPoint(scrX, scrZ);
    }
    if (dot.y && dot.z && dot.y.val && dot.z.val) {
        ctx.lineWidth = 1;
        ctx.strokeStyle='#999999';
        ctx.setLineDash(LINE_STYLE_DASHED);
        drawLine(scrX, scrY, scrX, scrZ);
    }
}

function tryDrawInterval(start, end) {
    ctx.lineWidth = 2;
    ctx.strokeStyle='#000000';
    ctx.setLineDash(LINE_STYLE_SOLID);

    if (start && start.y && end && end.y && end.y.val) {
        let startX = toScrX(start.x.val);
        let startY = toScrY(start.y.val);        
        let endX = toScrX(end.x.val);
        let endY = toScrY(end.y.val);
        drawLine(startX, startY, endX, endY);
    }
    if (start && start.z && end && end.z && end.z.val) {
        let startX = toScrX(start.x.val);
        let startZ = toScrZ(start.z.val);        
        let endX = toScrX(end.x.val);
        let endZ = toScrZ(end.z.val);
        drawLine(startX, startZ, endX, endZ);
    }
}

function calcLineYbyX (x, x0, y0, vectX, vectY) {
    return (x - x0) * vectY / vectX + y0;
}
function calcLineXbyY (y, x0, y0, vectX, vectY) {
    return vectX * (y-y0) / vectY + x0;
}

function calcLineInBorder(x0, y0, vectX, vectY, minX, maxX, minY, maxY) {
    // Каноническое уравнение прямой: (х-х0)/vectX = (y-y0)/vectY

    let r = { x1: 0, y1: 0, x2: 0, y2: 0 };

    r.x1 = minX;
    r.y1 = calcLineYbyX(minX, x0, y0, vectX, vectY);
    if (r.y1 < minY) {
        r.y1 = minY;
        r.x1 = calcLineXbyY(r.y1, x0, y0, vectX, vectY);
    }
    if (r.y1 > maxY) {
        r.y1 = maxY;
        r.x1 = calcLineXbyY(r.y1, x0, y0, vectX, vectY);
    }

    r.x2 = maxX;
    r.y2 = calcLineYbyX(r.x2, x0, y0, vectX, vectY);
    if (r.y2 < minY) {
        r.y2 = minY;
        r.x2 = calcLineXbyY(r.y2, x0, y0, vectX, vectY);
    }
    if (r.y2 > maxY) {
        r.y2 = maxY;
        r.x2 = calcLineXbyY(r.y2, x0, y0, vectX, vectY);
    }    

    return r;
}

function tryDrawPlane (dot, planeVect, plane) {
    line = calcLineInBorder(dot.x.val, 
            plane.val == 'y' ? dot.y.val : dot.z.val,
            planeVect.xNorm.val,
            plane.val == 'y' ? planeVect.yNorm.val : planeVect.zNorm.val,
            0, canvasElement.clientWidth - 2*FRAME_OFFSET, FRAME_OFFSET, canvasElement.clientHeight/2 - FRAME_OFFSET
        );

    ctx.lineWidth = 1;
    ctx.strokeStyle='#333333';
    ctx.setLineDash(LINE_STYLE_DASHED);

    if (plane.val == 'y')
        drawLine(toScrX(line.x1), toScrY(line.y1), toScrX(line.x2), toScrY(line.y2));
    else
        drawLine(toScrX(line.x1), toScrZ(line.y1), toScrX(line.x2), toScrZ(line.y2));
}

function tryDrawCircle(circleObj) {
    ctx.lineWidth = 2;
    ctx.strokeStyle='#000000';
    ctx.setLineDash(LINE_STYLE_SOLID);

    //console.log('cDraw', circleObj);

    if (circleObj.r && circleObj.r.val) {
        if (circleObj.plane.val == 'y') {
            if (circleObj.y && circleObj.y.val) drawCircle(toScrX(circleObj.x.val), toScrY(circleObj.y.val), circleObj.r.val);
            //if (circleObj.z) drawLine(toScrX(circleObj.x-circleObj.r), toScrZ(circleObj.z), toScrX(circleObj.x+circleObj.r), toScrZ(circleObj.z));
        }

        if (circleObj.plane.val == 'z') {
            if (circleObj.z && circleObj.z.val) drawCircle(toScrX(circleObj.x.val), toScrZ(circleObj.z.val), circleObj.r.val);
            //if (circleObj.y) drawLine(toScrX(circleObj.x-circleObj.r), toScrY(circleObj.y), toScrX(circleObj.x+circleObj.r), toScrY(circleObj.y));
        }

    }
}

function tryDrawIsoTriangle(baseObj, topObj) {
    ctx.lineWidth = 2;
    ctx.setLineDash(LINE_STYLE_SOLID);

    if (baseObj && baseObj.plane) {
        if (baseObj.plane == "y") {
            if (baseObj.z) {
                drawLine(toScrX(baseObj.x - baseObj.r), toScrZ(baseObj.z), toScrX(baseObj.x + baseObj.r), toScrZ(baseObj.z))
            }
            if (topObj.z) {
                drawLine(toScrX(baseObj.x - baseObj.r), toScrZ(baseObj.z), toScrX(baseObj.x), toScrZ(topObj.z));
                drawLine(toScrX(baseObj.x), toScrZ(topObj.z), toScrX(baseObj.x + baseObj.r), toScrZ(baseObj.z));
            }
        }

        if (baseObj.plane == "z") {
            if (baseObj.y) {
                drawLine(toScrX(baseObj.x - baseObj.r), toScrY(baseObj.y), toScrX(baseObj.x + baseObj.r), toScrY(baseObj.y))
            }
            if (topObj.y) {
                drawLine(toScrX(baseObj.x - baseObj.r), toScrY(baseObj.y), toScrX(baseObj.x), toScrY(topObj.y));
                drawLine(toScrX(baseObj.x), toScrY(topObj.y), toScrX(baseObj.x + baseObj.r), toScrY(baseObj.y));
            }
        }
    }

}

// функция проходится по всем ключевым точкам в объекте и пытается их отрисовать согласно типу 'type'
function drawObject(obj, isCur = false) {
    const keyPoints = obj.keyPoints;
    if (!keyPoints) return;

    if (isCur) {
        ctx.fillStyle = 'red';
        POINT_SIZE = 6;
    } else {
        ctx.fillStyle = 'black';
        POINT_SIZE = 3;
    }

    for (const [name, keyObj] of Object.entries(keyPoints)) {
        //console.log('f', name, keyObj)
        switch (keyObj.type.val) {
            case 'dot':
                tryDrawPoint(keyObj);
                break;
            case 'interval':
                tryDrawInterval(keyObj.start, keyObj.end);
                break;
                
            case 'plane':
                tryDrawPlane(keyObj.dot, keyObj.planeVect, keyObj.plane);
                break;
                
            case 'pplane':
                tryDrawPPlane(obj.keyPoints[0]);
                break;

            case 'circle':
                tryDrawCircle(keyObj);
                break;

            case 'isoTriangle':
                tryDrawIsoTriangle(obj.keyPoints[keyObj.base], obj.keyPoints[keyObj.top]);
                break;
        }

    }
}


function drawPlaneLine() {
    ctx.lineWidth = 1;
    ctx.setLineDash(LINE_STYLE_SOLID);
    ctx.strokeStyle = "black";
    ctx.lineCap = "butt";

    // пришлось добавить 0.5, чтобы линия была по настоящему тонкая
    drawLine(canvasElement.width - FRAME_OFFSET, canvasElement.height / 2 + 0.5, FRAME_OFFSET, canvasElement.height / 2 + 0.5);

    /// Создаем новый путь и задаем начальную точку на конце линии
    ctx.beginPath();
    ctx.moveTo(FRAME_OFFSET, canvasElement.height / 2 + 0.5);

    // Рисуем треугольник-стрелку на конце линии
    ctx.lineTo(FRAME_OFFSET + 5, canvasElement.height / 2 - 5);
    ctx.lineTo(FRAME_OFFSET + 5, canvasElement.height / 2 + 5);
    ctx.closePath();
    ctx.fillStyle = "black";
    ctx.fill();
}

// Для действий, которые происходят после загрузки страницы
addEventListener("DOMContentLoaded", (event) => {
    // Подготовительные действия по нахождению элемента для рисования и его запоминания.
    canvasElement = document.getElementById('drawingPlane');
    canvasElement.width = canvasElement.parentElement.clientWidth;
    canvasElement.height = canvasElement.parentElement.clientHeight;

    ctx = canvasElement.getContext("2d");
    redrawCanvas();
});