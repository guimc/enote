
// Список наших возможных инструментов со сценариями создания по стадиям каждого элемента
const drawInstruments = {
    'dot': {
        // стадии создания нашего объекта в редакторе
        stages: [
            {   // Первая стадия (первый клик) 
                hint: "Первый клик начнёт создание точки",  // Подсказка для редактора, что будет значить клик
                samePlane: 0,    // Должен ли быть клик на той же плоскости; 0 - не важно; 1 - на той же плоскости; -1 - на другой плоскости
                sets: [ // массив "установок", т.е. значений координат, который мы установим на этой стадии (за этот клик)
                    // keyPath - путь в труктуре нашего объекта, куда мы сохраняем координаты для построения. Пока ключевая точка у нашего объекта только одна - dot. А у неё - 3 координаты могут быть
                    // value - что положить в эту структуру на нашей установке: keyPath: "dot.x", value: "x" - означет положить пришедший х в объект dot.x
                    { forPlane: 0, keyPath: "dot.type", value: "\"dot\"" }, // задаём тип этого объекта, чтобы потом знать, как его рисовать
                    { forPlane: 0, keyPath: "dot.x", value: "x" }, // forPlane - для какого типа плоскости мы можем установить эту переменную. Для Х это не важно(=0) - его мы получаем на любой плоскости
                    { forPlane: 1, keyPath: "dot.y", value: "y" }, // координату Y мы можем получить только на П.1(нижняя), т.е. Y берём, только если клик был туда
                    { forPlane: 2, keyPath: "dot.z", value: "z" }, // координату Z, соответственно мы можем получить только на П.2(верхняя)
                ]
            },
            {
                hint: "Второй клик на другой плоскости для задания недостоющей координаты Y или Z",
                // Для второй стадии уже важно, чтобы клик был на плоскости, отличной от предыдущей стадии, потому стави plane: -1 и будем это проверять
                samePlane: -1, // Если клик прийдёт на ту же плоскость - мы её проигнорируем
                sets: [
                    { forPlane: 1, keyPath: "dot.y", value: "y" }, // по аналогии с первой стадией добираем недостоющую координату
                    { forPlane: 2, keyPath: "dot.z", value: "z" }, // сработает только одно из этих двух правил
                ]
            }
        ]
    },
    'interval': {
        stages: [
            {   // 1. Первая стадия (первый клик) 
                hint: "Первый клик задает начальную точку отрезка",
                samePlane: 0,
                sets: [
                    { forPlane: 0, keyPath: "dot1.type", value: "\"dot\"" },
                    { forPlane: 0, keyPath: "dot1.x", value: "x" },
                    { forPlane: 1, keyPath: "dot1.y", value: "y" },
                    { forPlane: 2, keyPath: "dot1.z", value: "z" },

                    // задаём как дополнительный ключевой объект - саму линию, соединяющую две точки
                    { forPlane: 0, keyPath: "interval.type", value: "\"interval\"" },

                    { forPlane: 0, keyPath: "interval.start.x", value: "x" },
                    { forPlane: 1, keyPath: "interval.start.y", value: "y" },
                    { forPlane: 2, keyPath: "interval.start.z", value: "z" },
                ]
            },
            {   // 3. Третья стадия (третий клик)
                hint: "Второй клик на любой плоскости, задаём первую точка конца отрезка",
                samePlane: 1,
                sets: [
                    { forPlane: 0, keyPath: "dot2.type", value: "\"dot\"" },
                    { forPlane: 0, keyPath: "dot2.x", value: "x" },
                    { forPlane: 1, keyPath: "dot2.y", value: "y" },
                    { forPlane: 2, keyPath: "dot2.z", value: "z" },

                    { forPlane: 0, keyPath: "interval.end.x", value: "x" },
                    { forPlane: 1, keyPath: "interval.end.y", value: "y" },
                    { forPlane: 2, keyPath: "interval.end.z", value: "z" },
                ]
            },
            {  // 2. Вторая стадия (второй клик) 
                hint: "Третий клик задает начальную точку отрезка на другой плоскости",
                samePlane: -1,
                sets: [
                    { forPlane: 1, keyPath: "dot1.y", value: "y" },
                    { forPlane: 2, keyPath: "dot1.z", value: "z" },

                    { forPlane: 1, keyPath: "interval.start.y", value: "y" },
                    { forPlane: 2, keyPath: "interval.start.z", value: "z" },
                ]
            },
            {   // 4. Четвертая стадия (четвертый клик)
                hint: "Четвертый клик другой плоскости, задаёт конец отрезка",
                samePlane: 1,
                sets: [
                    // {forPlane: 0, keyPath: "dot2.x", value: "x"}, на втором клике нет смысла задывать Х, он уже был задан на первом
                    { forPlane: 1, keyPath: "dot2.y", value: "y" },
                    { forPlane: 2, keyPath: "dot2.z", value: "z" },

                    { forPlane: 1, keyPath: "interval.end.y", value: "y" },
                    { forPlane: 2, keyPath: "interval.end.z", value: "z" },
                ]
            }
        ]
    },
    'cone': {
        stages: [
            {   // 1
                hint: "Первый клик задает центр основания конуса",
                samePlane: 0,
                sets: [
                    { forPlane: 0, keyPath: "baseDot.type", value: "\"dot\"" },
                    { forPlane: 0, keyPath: "baseDot.x", value: "x" },
                    { forPlane: 1, keyPath: "baseDot.y", value: "y" },
                    { forPlane: 2, keyPath: "baseDot.z", value: "z" },

                    { forPlane: 0, keyPath: "baseCircle.type", value: "\"circle\"" },
                    { forPlane: 0, keyPath: "baseCircle.x", value: "x" },

                    { forPlane: 1, keyPath: "baseCircle.y", value: "y" },
                    { forPlane: 1, keyPath: "baseCircle.plane", value: "\"y\"" },

                    { forPlane: 2, keyPath: "baseCircle.z", value: "z" },
                    { forPlane: 2, keyPath: "baseCircle.plane", value: "\"z\"" },
                ]
            },
            {   // 2
                hint: "Второй клик чтобы задать радиус основания",
                samePlane: 1,
                sets: [
                    { forPlane: 1, keyPath: "baseCircle.r", value: "Math.sqrt(Math.pow(curObject.keyPoints.baseCircle.x.val-x, 2) + Math.pow(curObject.keyPoints.baseCircle.y.val-y, 2))" },
                    { forPlane: 2, keyPath: "baseCircle.r", value: "Math.sqrt(Math.pow(curObject.keyPoints.baseCircle.x.val-x, 2) + Math.pow(curObject.keyPoints.baseCircle.z.val-z, 2))" },

                    
                    { forPlane: 0, keyPath: "baseInterval.type", value: "\"interval\"" },
                    { forPlane: 0, keyPath: "baseInterval.start.x", value: "curObject.keyPoints.baseCircle.x.val - curObject.keyPoints.baseCircle.r.val" },
                    { forPlane: 0, keyPath: "baseInterval.end.x", value: "curObject.keyPoints.baseCircle.x.val + curObject.keyPoints.baseCircle.r.val" },

                    { forPlane: 0, keyPath: "sideInterval1.type", value: "\"interval\"" },
                    { forPlane: 0, keyPath: "sideInterval1.start.x", value: "curObject.keyPoints.baseCircle.x.val - curObject.keyPoints.baseCircle.r.val" },
                    { forPlane: 0, keyPath: "sideInterval1.end.x", value: "curObject.keyPoints.baseCircle.x.val" },

                    
                    { forPlane: 0, keyPath: "sideInterval2.type", value: "\"interval\"" },
                    { forPlane: 0, keyPath: "sideInterval2.start.x", value: "curObject.keyPoints.baseCircle.x.val" },
                    { forPlane: 0, keyPath: "sideInterval2.end.x", value: "curObject.keyPoints.baseCircle.x.val + curObject.keyPoints.baseCircle.r.val" },
                ]
            },
            {   // 3
                hint: "Третий клик чтобы задать третью координату для основания на другой плоскости",
                samePlane: -1,
                sets: [
                    { forPlane: 1, keyPath: "baseDot.y", value: "y" },
                    { forPlane: 1, keyPath: "baseCircle.y", value: "y" },

                    { forPlane: 2, keyPath: "baseDot.z", value: "z" },
                    { forPlane: 2, keyPath: "baseCircle.z", value: "z" },

                    { forPlane: 1, keyPath: "baseInterval.start.y", value: "y" },
                    { forPlane: 2, keyPath: "baseInterval.start.z", value: "z" },
                    
                    { forPlane: 1, keyPath: "baseInterval.end.y", value: "y" },
                    { forPlane: 2, keyPath: "baseInterval.end.z", value: "z" },

                    { forPlane: 1, keyPath: "sideInterval1.start.y", value: "y" },
                    { forPlane: 2, keyPath: "sideInterval1.start.z", value: "z" },
                    
                    { forPlane: 1, keyPath: "sideInterval2.end.y", value: "y" },
                    { forPlane: 2, keyPath: "sideInterval2.end.z", value: "z" },
                ]
            },
            {   // 4
                hint: "Четвертый клик на той же плоскости, чтобы задать высоту конуса",
                samePlane: 1,
                sets: [
                    { forPlane: 1, keyPath: "sideInterval1.end.y", value: "y" },
                    { forPlane: 2, keyPath: "sideInterval1.end.z", value: "z" },
                    
                    { forPlane: 1, keyPath: "sideInterval2.start.y", value: "y" },
                    { forPlane: 2, keyPath: "sideInterval2.start.z", value: "z" },
                ]
            },
        ]
    },
    'plane': {
        stages: [
            {   // 1
                hint: "Первый клик для задания плоскости",
                samePlane: 0,
                sets: [
                    { forPlane: 0, keyPath: "dot1.type", value: "\"dot\"" },
                    { forPlane: 0, keyPath: "dot1.x", value: "x" },
                    { forPlane: 1, keyPath: "dot1.y", value: "y" },
                    { forPlane: 2, keyPath: "dot1.z", value: "z" },
                ]
            },
            {   // 2
                hint: "Второй клик для задания плоскости",
                samePlane: 1,
                sets: [
                    { forPlane: 0, keyPath: "dot2.type", value: "\"dot\"" },
                    { forPlane: 0, keyPath: "dot2.x", value: "x" },
                    { forPlane: 1, keyPath: "dot2.y", value: "y" },
                    { forPlane: 2, keyPath: "dot2.z", value: "z" },

                    { forPlane: 0, keyPath: "plane.dot.x", value: "x" },
                    { forPlane: 1, keyPath: "plane.dot.y", value: "y" },
                    { forPlane: 2, keyPath: "plane.dot.z", value: "z" },
                    
                    // просчитываем по двум точка вектор плоскости (dot1->dot2)
                    { forPlane: 0, keyPath: "plane.type", value: "\"plane\"" },
                    { forPlane: 1, keyPath: "plane.plane", value: "\"y\"" },
                    { forPlane: 2, keyPath: "plane.plane", value: "\"z\"" },

                    { forPlane: 0, keyPath: "plane.planeVect.x", value: "x-curObject.keyPoints.dot1.x.val" },
                    { forPlane: 1, keyPath: "plane.planeVect.y", value: "y-curObject.keyPoints.dot1.y.val" },
                    { forPlane: 2, keyPath: "plane.planeVect.z", value: "z-curObject.keyPoints.dot1.z.val" },

                    // вычисляем длину вектора
                    { forPlane: 1, keyPath: "plane.planeVect.len", value: "Math.sqrt(Math.pow(curObject.keyPoints.plane.planeVect.x.val,2) + Math.pow(curObject.keyPoints.plane.planeVect.y.val,2))" },
                    { forPlane: 2, keyPath: "plane.planeVect.len", value: "Math.sqrt(Math.pow(curObject.keyPoints.plane.planeVect.x.val,2) + Math.pow(curObject.keyPoints.plane.planeVect.z.val,2))" },
                    
                    // вычисляем соноправленный нормализованный вектор
                    { forPlane: 0, keyPath: "plane.planeVect.xNorm", value: "curObject.keyPoints.plane.planeVect.x.val / curObject.keyPoints.plane.planeVect.len.val" },
                    { forPlane: 1, keyPath: "plane.planeVect.yNorm", value: "curObject.keyPoints.plane.planeVect.y.val / curObject.keyPoints.plane.planeVect.len.val" },
                    { forPlane: 2, keyPath: "plane.planeVect.zNorm", value: "curObject.keyPoints.plane.planeVect.z.val / curObject.keyPoints.plane.planeVect.len.val" },

                ]
            },

        ]
    }
}